#!/usr/bin/env python3.6

import sys, re
from collections import Counter 
import csv
from bs4 import BeautifulSoup 
import requests
import json
from dateutil.parser import parse
import tweepy

# regex = sys.argv[1]

# for line in sys.stdin:
#     if re.search(regex, line):
#         sys.stdout.write(line)

# count = 0
# for line in sys.stdin:
#     count +=1

# print(count)

# try:
#     num_words = int(sys.argv[1])
# except:
#     print("usage: gettingdata.py num_words")
#     sys.exit(1)     # non-zero exit code indicates error

# counter = Counter(word.lower()
#                     for line in sys.stdin
#                     for word in line.strip().split()
#                     if word)

# for word, count in counter.most_common(num_words):
#     sys.stdout.write(str(count))
#     sys.stdout.write("\t")
#     sys.stdout.write(word)
#     sys.stdout.write("\n")

# file_for_reading = open("algebra.py", 'r')
# file_for_reading.close()

filename = "algebra.py"
starts_with_hash = 0
with open(filename, 'r') as f:
    for line in f:
        if re.match("^#", line):
            starts_with_hash += 1

# print(starts_with_hash)

def get_domain(email_address):
    """split on '@' and return the last piece"""
    return email_address.lower().split('@')[-1]

with open("../data/data.dat", 'r') as f:
    domains_count = Counter(get_domain(line.strip())
                            for line in f
                            if "@" in line)

with open("../data/stocks.tsv", newline='\n') as f:
    reader = csv.reader(f, delimiter='\t')
    # print(reader)
    for row in reader:
        date = row[0]
        symbol = row[1]
        closing_price = float(row[2])
        # print( date, symbol, closing_price)

with open("../data/hash", newline='\n') as f:
    reader = csv.DictReader(f, delimiter=':')
    for row in reader:
        date = row['date']
        symbol = row['symbol']
        closing_price = float(row['closing_price'])
        # print(date, symbol, closing_price)

today_prices = { 'AAPL': 90.91, 'MSFT': 41.68, 'FB': 64.5 }

with open('../data/comma_delimited_stock_prices.txt', 'w') as f:
    writer = csv.writer(f, delimiter=',')
    for stock, price in today_prices.items():
        writer.writerow([stock, price])

html = requests.get("http://www.example.com").text
soup = BeautifulSoup(html, 'html5lib')


first_paragraph = soup.find('p')
first_paragraph_text = soup.p.text
first_paragraph_words = first_paragraph_text.split()

# first_paragraph_id = soup.p['id']   # raises KeyError if no 'id'
first_paragraph_id2 = soup.p.get('id')  # returns None if no 'id'

all_paragraphs = soup.find_all('p') # or just soup('p')
paragraphs_with_ids = [p for p in soup('p') if p.get('id')]

important_paragraphs = soup('p', {'class': 'important'})
important_paragraphs2 = soup('p', 'important')
important_paragraphs3 = [p for p in soup('p')
                            if 'important' in p.get('class', [])]

# print(all_paragraphs)
# print(important_paragraphs3)

spans_inside_divs = [span
                    for div in soup('div')  # for each <div> on the page
                    for span in div('span') # find each <span> inside it
                    ]

# print(spans_inside_divs)

serialized = """{ "title" : "Data Science Book",
"author" : "Joel Grus",
"publicationYear" : 2014,
"topics" : [ "data", "science", "data science"] }"""

# parse the JSON to create a Python dict
deserialized = json.loads(serialized)
if "data science" in deserialized["topics"]:
    ''
    # print(deserialized)

endpoint = "https://api.github.com/users/gnaneshkunal/repos"
repos = json.loads(requests.get(endpoint).text)
# print(repos)

dates = [parse(repo["created_at"]) for repo in repos]
# print(dates)
months_counts = Counter(date.month for date in dates)
weekday_counts = Counter(date.weekday() for date in dates)

last_5_repositories = sorted(repos,
                                key=lambda r: r["created_at"],
                                reverse=True)[:5]
last_5_languages = [repo["language"]
                    for repo in last_5_repositories]
# print(last_5_languages)

auth = tweepy.OAuthHandler("", "")
auth.set_access_token("", "")

api = tweepy.API(auth)

# public_tweets = api.home_timeline()
# for tweet in public_tweets:
#     print(tweet.text)
search_tweets = api.search("data science")
for tweet in search_tweets:
    ''# print(tweet.text)



