#!/usr/bin/env python3.6

from collections import Counter, defaultdict
import math
from matplotlib import pyplot as plt
import random
from probability import inverse_normal_cdf
from stats import correlation, mean, standard_deviation
import csv
import dateutil
import datetime
from functools import reduce, partial
from algebra import distance, magnitude, dot, vector_sum, scalar_multiply, vector_subtract, get_col, shape, make_matrix
from gradient import maximize_batch, maximize_stochastic


def bucketize(point, bucket_size):
    """floor the point to the next lower multiple of bucket_size"""
    return bucket_size * math.floor(point / bucket_size)

def make_histogram(points, bucket_size):
    """buckets the points and counts how many in each bucket"""
    return Counter(bucketize(point, bucket_size) for point in points)

def plot_histogram(points, bucket_size, title=""):
    histogram = make_histogram(points, bucket_size)
    # print(histogram)
    plt.bar(histogram.keys(), histogram.values(), width=bucket_size)
    plt.title(title)
    plt.show()

random.seed(0)

# uniform between -100 and 100
uniform = [200 * random.random() - 100 for _ in range(10000)]

# normal distribution with mean 0, standard deviation 57
normal = [57 * inverse_normal_cdf(random.random())
            for _ in range(10000)]

# plot_histogram(uniform, 10, "uniform Histogram")
# plot_histogram(normal, 10, "Normal Histogram")

def random_normal():
    """returns a random draw from a standard normal distribution"""
    return inverse_normal_cdf(random.random())

xs = [random_normal() for _ in range(1000)]
ys1 = [x + random_normal() / 2 for x in xs]
ys2 = [-x + random_normal() / 2 for x in xs]
# print(xs)
# print(ys1)
# print(ys2)
# plt.scatter(xs, ys1, marker='.', color='black', label='ys1')
# plt.scatter(xs, ys2, marker='.', color='grey', label='ys2')
# plt.xlabel('xs')
# plt.ylabel('ys')
# plt.legend(loc=9)
# plt.title('Very Differenct Joint Distribution')
# plt.show()

# print(correlation(xs, ys1))
# print(correlation(xs, ys2))

def correlation_matrix(data):
    """returns the num_columns x num_columns matrix whose (i, j)th entry
    is the correlation between columns i and j of data"""
    _, num_columns = shape(data)

    def matrix_entry(i, j):
        return correlation(get_col(data, i), get_col(data, i))

    return make_matrix(num_columns, num_columns, matrix_entry)

# def parse_row(input_row, parsers):
#     """given a list of parsers (some of which may be None)
#     apply the appropriate one to each element of the input_row"""
#     return [parser(value) if parser is not None else value
#             for value, parser in zip(input_row, parsers)]

def parse_rows_with(reader, parsers):
    """wrap a reader to apply the parsers to each of its rows"""
    for row in reader:
        yield parse_row(row, parsers)

def try_or_none(f):
    """wraps f to return None if f raises an exception
    assumes f takes only one input"""
    def f_or_none(x):
        try: return f(x)
        except: return None
    return f_or_none

def parse_row(input_row, parsers):
    """given a list of parsers (some of which may be None)
    apply the appropriate one to each element of the input_row"""
    return [try_or_none(parser)(value) if parser is not None else value
            for value, parser in zip(input_row, parsers)]

data = []
with open("../data/stocks3.tsv", newline='\n') as f:
    reader = csv.reader(f, delimiter='\t')
    for line in parse_rows_with(reader, [None, dateutil.parser.parse, float]):
        data.append(line)

# print(data)

def try_parse_field(field_name, value, parser_dict):
    """try to parse value using the appropriate function from parser_dict"""
    parser = parser_dict.get(field_name)
    if parser is not None:
        return try_or_none(parser)(value)
    else:
        return value

def parse_dict(input_dict, parser_dict):
    return { field_name: try_parse_field(field_name, value, parser_dict)
            for field_name, value in input_dict.iteritems() }

def picker(field_name):
    """returns a function that picks a field out of a dict"""
    return lambda row: row[field_name]

def pluck(field_name, rows):
    """turn a list of dicts into the list of field_name values"""
    return map(picker(field_name), rows)

def group_by(grouper, rows, value_transform=None):
    # key is output of grouper, value is list of rows
    grouped = defaultdict(list)
    for row in rows:
        grouped[grouper(row)].append(row)
    
    if value_transform is None:
        return grouped
    else:
        return { key: value_transform(rows)
                for key, rows in grouped.items() }

data_dict = [{'closing_price': cp, 'date': date, 'symbol': s} for s, date, cp in data]

max_price_by_symbol = group_by(picker("symbol"),
                                data_dict,
                                lambda rows: max(pluck("closing_price", rows)))

# print(max_price_by_symbol)
# print(data_dict)

# print(list(pluck('closing_price', [{'closing_price': 90.91, 'date': datetime.datetime(2014, 6, 20, 0, 0), 'symbol': 'AAPL'}, {'closing_price': 41.68, 'date': datetime.datetime(2014, 6, 20, 0, 0), 'symbol': 'MSFT'}, {'closing_price': 64.5, 'date': datetime.datetime(2014, 6, 20, 0, 0), 'symbol': 'FB'}, {'closing_price': 91.86, 'date': datetime.datetime(2014, 6, 19, 0, 0), 'symbol': 'AAPL'}, {'closing_price': 41.51, 'date': datetime.datetime(2014, 6, 19, 0, 0), 'symbol': 'MSFT'}, {'closing_price': 64.34, 'date': datetime.datetime(2014, 6, 19, 0, 0), 'symbol': 'FB'}])))
# print(picker("closing_price")({'closing_price': 90.91, 'date': datetime.datetime(2014, 6, 20, 0, 0), 'symbol': 'AAPL'}))

def percent_price_change(yesterday, today):
    return today['closing_price'] / yesterday['closing_price'] - 1

def day_over_day_changes(grouped_rows):
    # sort the rows by date
    ordered = sorted(grouped_rows, key=picker('date'))

    # zip with an offset to get pairs of consecutive days
    return [{ 'symbol' : today['symbol'],
                'date': today['date'],
                'change': percent_price_change(yesterday, today)}
            for yesterday, today in zip(ordered, ordered[1:])]
# key is symbol, value is list of 'change' dicts
changes_by_symbol = group_by(picker('symbol'), data_dict, day_over_day_changes)

# print(changes_by_symbol)
# collect all 'change' dicts into one big list
all_changes = [change
                for changes in changes_by_symbol.values()
                for change in changes]
# print(all_changes)
# print(max(all_changes, key=picker('change')))
# print(min(all_changes, key=picker('change')))

# to combine percent changes, we add 1 to each, multiply them, and subtract 1
# for instance, if we combine +10% and -20%, the overall change is
#       (1 + 10%) * (1 - 20%) - 1 = 1.1 * .8 - 1 = -12%

def combine_pct_changes(pct_change1, pct_change2):
    return (1 + pct_change1) * (1 + pct_change2) - 1

def overall_change(changes):
    return reduce(combine_pct_changes, pluck('change', changes))

overall_change_by_month = group_by(lambda row: row['date'].month,
                                    all_changes,
                                    overall_change)
# print(overall_change_by_month)

height_data = {
    'a': {'hi': 63, 'hci': 160, 'w': 150 },
    'b': {'hi': 67, 'hci': 170.2, 'w': 160 },
    'c': {'hi': 70, 'hci': 177.8, 'w': 171 }
}
def h_get(cond, col1, col2):
    def h_and_w(row, col1, col2):
        return [row[col1], row[col2]]
    return list(h_and_w(x, col1, col2) for n, x in height_data.items() if n == cond)[0]

a_to_b = distance(
    h_get('a', 'hi', 'w'), 
    h_get('b', 'hi', 'w')
)

a_to_c = distance(
    h_get('a', 'hi', 'w'),
    h_get('c', 'hi', 'w')
)

b_to_c = distance(
    h_get('b', 'hi', 'w'),
    h_get('c', 'hi', 'w')
)
# print(a_to_b)

a_to_b_centi = distance(
    h_get('a', 'hci', 'w'),
    h_get('b', 'hci', 'w')
)

a_to_c_centi = distance(
    h_get('a', 'hci', 'w'),
    h_get('c', 'hci', 'w')
)

b_to_c_centi = distance(
    h_get('b', 'hci', 'w'),
    h_get('c', 'hci', 'w')
)


# for hi, _, w in height_data.items():
#     print(hi, w)

# x = [[x['hi'], x['w']] for y, x in height_data.items() if y == 'a']


# y = list(map(h_and_w, height_data.values()))
# print(list(h_and_w(x) for n, x in height_data.items() if n == 'a')[0])

# print(x)
# for x, y in height_data.items():
#     print([y['hi'], y['w']])

# print(list(hi, w) for n, hi, _, w in height_data)

def scale(data_matrix):
    """returns the means and standard deviations of each column"""
    num_rows, num_cols = shape(data_matrix)
    means = [mean(get_col(data_matrix, j))
            for j in range(num_cols)]
    stdevs = [standard_deviation(get_col(data_matrix, j))
            for j in range(num_cols)]
    return means, stdevs

def rescale(data_matrix):
    """rescales the input data so that each column
    has mean 0 and standard deviation 1
    leaves alone columns with no deviation"""
    means, stdevs = scale(data_matrix)

    def rescaled(i, j):
        if stdevs[j] > 0:
            return (data_matrix[i][j] - means[j]) / stdevs[j]
        else:
            return data_matrix[i][j]
    num_rows, num_cols = shape(data_matrix)
    return make_matrix(num_rows, num_cols, rescaled)

def de_mean_matrix(A):
    """returns the result of subtracting from every value in A the mean
    value of its column. the resulting matrix has mean 0 in every column"""
    nr, nc = shape(A)
    columns_mean, _ = scale(A)
    return make_matrix(nr, nc, lambda i, j: A[i][j] - column_means[j])

def direction(w):
    mag = magnitude(w)
    return [w_i / mag for w_i in w]

def directional_variance_i(x_i, w):
    """the variance of the row x_i in the direction determined by w"""
    return dot(x_i, direction(w)) ** 2

def directional_variance(x, w):
    """the variance of the data in the direction determined w"""
    return sum(directional_variance_i(x_i, w)
                for x_i in x)

def directional_variance_gradient_i(x_i, w):
    """the contribution of row x_i to the gradient of
    the direction-w variance"""
    projection_length = dot(x_i, direction(w))
    return [2 * projection_length * x_ij for x_ij in x_i]

def directional_variance_gradient(X, w):
    return vector_sum(directional_variance_gradient_i(x_i, w)
                for x_i in X)

def first_principal_component(X):
    guess = [1 for _ in X[0]]
    unscaled_maximizer = maximize_batch(
        partial(directional_variance, X),           # is now a function of w
        partial(directional_variance_gradient, X),   # is now a function of w
        guess)
    return direction(unscaled_maximizer)

# here there is no 'y', so we just pass in a vector of Nones
# and the functions that ignore that input

def first_principal_component_sgd(X):
    guess = [1 for _ in X[0]]
    unscaled_maximizer = maximize_stochastic(
        lambda x, _, w: directional_variance_i(x, w),
        lambda x, _, w: directional_variance_gradient_i(x, w),
        X,
        [None for _ in X],  # the fake 'y'
        guess)
    return direction(unscaled_maximizer)

def project(v, w):
    """return the projection of v onto the direction w"""
    projection_length = dot(v, w)
    return scalar_multiply(projection_length, w)

def remove_projection_from_vector(v, w):
    """projects v onto w and subtracts the result from v"""
    return vector_subtract(v, project(v, w))

def remove_projection(X, w):
    """for each row of X
    projects the row onto w, and subtracts the result from the row"""
    return [remove_projection_from_vector(x_i, w) for x_i in x]

def principal_component_analysis(X, num_components):
    components = []
    for _ in range(num_components):
        component = first_principal_component(x)
        components.append(component)
        X = remove_projection(X, component)
    return components

def transform_vector(v, components):
    return [dot(v, w) for w in components]

def transform(X, components):
    return [transform_vector(x_i, components) for x_i in X]









