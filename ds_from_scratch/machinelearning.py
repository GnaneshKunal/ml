#!/usr/bin/env python3.6

import random

# print("Machine Learning")

def split_data(data, prob):
    """split data into fractions [prob, 1 - prob]"""
    results = [], []
    for row in data:
        results[0 if random.random() < prob else 1].append(row)
    return results

def train_test_split(x, y, test_pct):
    data = zip(x, y)                        # pair corresponding values
    train, test = split_data(data, 1 - test_pct) # split the data set of pairs
    x_train, y_train = zip(*train)          # magical un-zip trick
    x_test, y_test = zip(*test)
    return x_train, x_test, y_train, y_test

#               we might do something like                  #
# model = SomeKindOfModel()
# x_train, x_test, y_train, y_test = train_test_split(xs, ys, 0.33)
# model.train(x_train, y_train)
# performance = model.test(x_test, y_test)

#           tp  fp      fn      tn
luke_mia = (70, 4930, 13930, 981070)

def accuracy(tp, fp, fn, tn):
    correct = tp + tn
    total = tp + fp + fn + tn
    return correct / total

# print(accuracy(*luke_mia))

def precision(tp, fp, fn, tn):
    return tp / (tp + fp)

# print(precision(*luke_mia))

def recall(tp, fp, fn, tn):
    return tp / (tp + fn)

# print(recall(*luke_mia))

def f1_score(tp, fp, fn, tn):
    p = precision(tp, fp, fn, tn)
    r = recall(tp, fp, fn, tn)
    return 2 * p * r / (p + r)

# print(f1_score(*luke_mia))


