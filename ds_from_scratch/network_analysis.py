#!/usr/bin/env python3

users_data = ["Hero", "Dunn", "Sue", "Chi", "Thor", "Clive", "Hicks", "Devin", "Kate", "Klein"]
users = [{ "id": id, "name": name } for id, name in enumerate(users_data)]

friendships = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),
                (4, 5), (5, 6), (5, 7), (6, 8), (7, 8), (8, 9)]

if __name__ == "__main__":
    print("Network Analysis")
    
    for user in users:
        user["friends"] = []
    
    for i, j in friendships:
        # this works because users[i] is the user whose id is i
        users[i]["friends"].append(users[j])    # add i as a friend of j
        users[j]["friends"].append(users[j])    # add j as a friend of i
    
    print(users)

