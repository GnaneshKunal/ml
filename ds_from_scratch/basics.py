#!/usr/bin/env python3

# import this
import re
import matplotlib.pyplot as plt
from collections import defaultdict, Counter
#from __future__ import division # only if you write py2
from collections import defaultdict, Counter
import random
from functools import partial, reduce

# print(this)

for i in [1, 2, 3, 4, 5]:
    # print(i)
    for j in [1, 2, 3, 4, 5]:
        ''
    #     print(j)
    #     print (i + j)
    # print(i)
print("done looping")

long_winded_computation = (1 + 2 + 
                            3 + 4)

list_of_lists = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

easier_to_read_list_of_lists = [ [1, 2, 3],
                                 [4, 5, 6],
                                 [7, 8, 9] ]

two_plus_three = 2 + \
                    3
my_regex = re.compile("[0-9]+", re.I)
lookup = defaultdict(int)
my_counter = Counter()

def double(x):
    """this is where you put an optional docstring
    this function mutiplies its input by 2
    """
    return x * 2

def apply_to_one(f):
    """calls the function f with 1 as its argument"""
    return f(1)

my_double = double
x = apply_to_one(my_double)
print(x)

y = apply_to_one(lambda x: x + 4)
print(y)

def my_print(message="my default message"):
    print(message)
my_print(message="ADS")

def subtract(a=0, b=0):
    return a - b
print(subtract(10, 5))
print(subtract(b=5))

single_quoted_string = 'data science'
double_quoted_string = "data science"

tab_string = "\t"
print(len(tab_string))

not_tab_string = r"\t" # represents the character `\` and `t`
print(len(not_tab_string))

multi_line_string = """this is the first line.
and this is the second line
and this is the third line"""
print(multi_line_string)

try:
    print (0 / 0)
except ZeroDivisionError:
    print("can't divide by zero")

integer_list = [1, 2, 3]
hetrogeneous_list = ["string", .1, True]
list_of_lists = [integer_list, hetrogeneous_list, []]

list_length = len(integer_list)
list_sum = sum(integer_list)

x = range(10) # [0, ..., 9]
zero = x[0]
nine = x[-1]

first_three = x[:3]
one_to_four = x[1:5]
copy_of_x = x[:]

print(copy_of_x)
print(1 in [1, 2, 3])

x = [1, 2, 3]
x.extend([4, 5, 6])
print(x)

x = [1, 2, 3]
y = x + [4, 5, 6]
print(y)

x = [1, 2, 3]
x.append(0)
print(x)

x, y = [1, 2]
print(x, y)

_, y = [3, 4] # now y == 4, didn't car about the first element
print(y)

my_list = [1, 2]
my_tuple = (1, 2)
other_tuple = 3, 4
my_list[1] = 3

try:
    my_tuple[1] = 3
except TypeError:
    print("cannot modify a tuple")

def sum_and_product(x, y):
    return (x + y), (x * y)

sp = sum_and_product(2, 3)
print(sp)
s, p = sum_and_product(5, 10)
print(s, p)

empty_dict = {} # pythonic
empty_dict2 = dict() # less pythonic
grades = {"Joel": 80, "Tim": 95} # dictionary literal
joels_grade = grades["Joel"]

# KeyError
try:
    kate_grade = grades["Kate"]
except KeyError:
    print("no grade for kate")

joel_has_grades = "Joel" in grades
print(joel_has_grades)

kates_grade = grades.get("Kate", 0) # equals 0

grades["Time"] = 90
num_student = len(grades)

tweet = {
    "user": "joelgrus",
    "text": "Data Science is Awesome",
    "retweet_count": 100,
    "hashtags": ["#data", "#science", "#datascience", "#awesome", "#yolo"]
}

tweet_keys = tweet.keys() # list of keys
tweet_values = tweet.values() # list of valeus
tweet_items = tweet.items() # list ofg (key, value) tuples

"user" in tweet_keys

document = ["This", "DSA", "SDAA", "This", "DSA"]

word_counts = {}
for word in document:
    if word in word_counts:
        word_counts[word] += 1
    else:
        word_counts[word] = 1
print(word_counts)

word_counts = {}
for word in document:
    try:
        word_counts[word] += 1
    except KeyError:
        word_counts[word] = 1

word_counts = {}
for word in document:
    previous_count = word_counts.get(word, 0)
    word_counts[word] = previous_count + 1

word_counts = defaultdict(int)
for word in document:
    word_counts[word] += 1

dd_list = defaultdict(list)
dd_list[2].append(1) # now dd_list contains {2: [1]}

dd_list = defaultdict(dict)
dd_list["Joel"]["City"] = "Seattle"

dd_pair = defaultdict(lambda: [0, 0])
dd_pair[2][1] = 1
dd_pair[0][0] = 1
dd_pair[0][1] = 2
print(dd_pair)

c = Counter([0, 1, 2, 0])
print(c)

word_counts = Counter(document)
print(word_counts)

# print the 2 most common words and their counts
for word, count in word_counts.most_common(2):
    print(word, count)

s = set()
s.add(1) # { 1 }
s.add(2) # { 1, 2 }
s.add(2) # still { 1, 2 }

y = 2 in s # True 

stopwords_list = ["a", "an", "at"] + ["the", "but"] + ["yet", "you"]
"zip" in stopwords_list # False, but have to check every element

stopwords_set = set(stopwords_list)
"zip" in stopwords_set # very fast to check

item_list = [1, 2, 3, 4, 1, 2]
num_items = len(item_list)
print(num_items)

item_set = set(item_list) # 6
num_distinct_items = len(item_set) # {1, 2, 3, 4}
distinct_item_list = list(item_set) #[1, 2, 3, 4]

if 1 > 2:
    message = "if only 1 were greater than two..."
else:
    message = "when all else fails use else (if you want to)"

parity = "even" if x % 2 == 0 else "odd"

x = 0
while x < 10:
    print(x, "is less than 10")
    x += 1

for x in range(10):
    print(x, "is less than 10")

for x in range(10):
    if x == 3:
        continue # go immediately to the next iteration
    if x == 5:
        break # quit the loop entirely
    print(x)

one_is_less_than_two = 1 < 2
true_equals_false = True == False

x = None
print(x == None)
print(x is None)

x = [4, 1, 2, 3]
y = sorted(x)
x.sort()

x = sorted([ -4, 1, -2, 3], key=abs, reverse=True)
print(x)

wc = sorted(word_counts.items(), key=lambda x: x[1], reverse=True)
print(wc)

even_numbers = [x for x in range(10) if x % 2 == 0]
print(even_numbers)
squares = [x * x for x in range(5)]
even_squares = [x * x for x in even_numbers]

print(even_squares)

square_dict = { x: x * x for x in range(5) }
print(square_dict.items())
square_set = { x * x for x in [-1, 1]}
print(square_set)

zeros = [0 for _ in even_numbers]
print(zeros)

paris = [(x, y)
            for x in range(10)
            for y in range(10)]

print(paris)

increasing_pairs = [(x, y) 
                        for x in range(10)
                        for y in range(x + 1, 10)]

print(increasing_pairs)

def lazy_range(n):
    """a lazy version of range"""
    i = 0
    while i < n:
        yield i
        i += 1

for i in lazy_range(10):
    ''

def natural_numbers():
    """returns 1, 2, 3, ..."""
    n = 1
    while True:
        yield n
        n += 1

lazy_evens_below_20 = (i for i in lazy_range(20) if i % 2 == 0)

for x in lazy_evens_below_20:
    print(x)

four_uniform_randoms = [random.random() for _ in range(4)]
print(four_uniform_randoms)

random.seed(10)
print(random.random())
random.seed(10)
print(random.random())

print(random.randrange(10)) # returns an element in range [0, ..., 10]
print(random.randrange(3, 6))

up_to_ten = list(range(10))
random.shuffle(up_to_ten)
print(up_to_ten)

my_best_friend = random.choice(["Moin", "Jashwanth", "Theja", "Sam", "Dheeraj"])
print(my_best_friend)

lottery_numbers = list(range(60))
winning_numbers = random.sample(lottery_numbers, 6)
print(winning_numbers)

# duplicates may exist
winning_numbers_with_dup = [random.choice(lottery_numbers)
                            for _ in range(6)] 
print(winning_numbers_with_dup)

#regular exp
print(all([                      # all of these are true, because
    not re.match("a", "cat"),   # * 'cat' doesn't start with 'a'
    re.search("a", "cat"),      # * 'cat' has an 'a' in it
    not re.search("c", "dog"),  # * 'dog' doesn't have a 'c' in it
    3 == len(re.split("[ab]", "carbs")), # * split on a or b to ['c', 'r', 's']
    "R-D-" == re.sub("[0-9]", "-", "R2D2") # * replace digits with dashes
])) # prints true

# print(not re.match("a", "cat"))
# print(not not re.search("a", "cat"))
# print(not re.search("c", "dog"))
# print(3 == len(re.split("[ab]", "carbs")))
# print("R-D-" == re.sub("[0-9]", "-", "R2D2"))

# print(all)

# by convention, we give classes PascalCase names
class Set:

    # these are the member functions
    # every one takes a first argument "self" (another convention)
    # that refers to the particular Set object being used

    def __init__(self, values=None):
        """This is the constructor.
        It gets called when you create a new Set.
        your would use it lie
        s1 = Set() # empty set
        s2 - Set([1, 2, 2, 3]) # initialize with values"""

        self.dict = {}  # each instance of Set has its own dict property
                        # which is what we'll use to track memberships
        if values is not None:
            for value in values:
                self.add(value)
    def __repr__(self):
        """this is the string representation of a Set object
        if you type it at the Python prompt or pass it to str()"""
        return "Set: " + str(self.dict.keys())

    # we'll represent membership by being a key in self.dict with value True
    def add(self, value):
        self.dict[value] = True
    
    # value is in the Set if it's a key in the dictionary
    def contains(self, value):
        return value in self.dict

    def remove(self, value):
        del self.dict[value]
        
s = Set([1, 2, 3])
s.add(4)
print(s)
print(s.contains(4))
s.remove(3)
s.add(4)
print(s.contains(3))

# partially apply (or curry) functions to create a new function

def exp(base, power):
    return base ** power

def two_to_the(power):
    return exp(2, power)

two_to_the_v2 = partial(exp, 2) # is no a function of one variable
print(two_to_the(3))
print(two_to_the_v2(3))

square_of = partial(exp, power=2)
print(square_of(2))

def double(x):
    return 2 * x
xs = [1, 2, 3, 4]
twice_xs = [double(x) for x in xs] # 2, 4, 6, 8
twice_xs = map(double, xs)
list_doubler = partial(map, double)
twice_xs = list_doubler(xs)
# for x in twice_xs:
#     print(x)
print(list(twice_xs))

def multiply(x, y): return x * y

products = map(multiply, [1, 2], [4, 5])    #[1 * 4, 2 * 5] = [4, 50]
print(list(products))

def is_even(x):
    """True if x is even, False if x is odd"""
    return x % 2 == 0
x_evens = [x for x in xs if is_even(x)]
x_evens = filter(is_even, xs)
print(list(x_evens))

list_evener = partial(filter, is_even)
x_evens = list_evener(xs)
print(list(x_evens))

x_product = reduce(multiply, xs)
list_product = partial(reduce, multiply)
x_product = list_product(xs)
print(x_product)

# enumerate
for i in range(len(document)): # not pythonic
    doc = document[i]
    # do_something(i, doc)

i = 0
for doc in document: # pythonic
    ''
    # do_something(i, doc) 

# pythonic
for i, doc in enumerate(document):
    ''
    # do_something(i, doc)

# also
for i in range(len(document)): '' # do_something(i) # not pythonic
for i, _ in enumerate(document): '' # do_something(i) # pythonic

list1 = ['a', 'b', 'c']
list2 = [1, 2, 3]
print(list(zip(list1, list2)))
# for x, y in zip(list1, list2):
#     print(x, y)

pairs = [('a', 1), ('b', 2), ('c', 3)]
letters, numbers = zip(*pairs)
print(letters, numbers)
# zip(*pairs) == zip(('a', 1), ('b', 2), ('c', 3))

def add(a, b): return a + b
print(add(1, 2))
print(add(*[1, 2]))

# args and kwargs

def doubler(f):
    def g(x):
        return 2 * f(x)
    return g
def f1(x):
    return x + 1
g = doubler(f1)
print(g(3))
print(g(-1))

def f2(x, y):
    return x + y

g = doubler(f2)
# print(g(1, 2)) # breaks g() takes exactly one arg

def magic(*args, **kwargs):
    print("unnamed args: ", args)
    print("keyword args: ", kwargs)

magic(1, 2, key="word", key2="word2")

def other_way_magic(x, y, z):
    return x + y + z

x_y_list = [1, 2]
z_dict = { "z": 3 }
print(other_way_magic(*x_y_list, **z_dict))

def doubler_correct(f):
    """words no matter what kind of inputs f  expects"""
    def g(*args, **kwargs):
        """whatever arguments g is supplied, pass them through to f"""
        return 2 * f(*args, **kwargs)
    return g

g = doubler_correct(f2)
print(g(1, 2))
