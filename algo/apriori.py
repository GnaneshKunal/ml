#!/usr/bin/env python3.6

from collections import defaultdict
from itertools import combinations, permutations

from functools import reduce
import random

def containsall(string, str_set):
    """
    Returns true if all the characters are present in the set
    """
    return 0 not in [c in string for c in str_set]

x = ["MONKEY", "DONKEY", "MAKE", "MUCKY", "COOKE"]

y = defaultdict(int)

support = 60

confidence = 80

sup = (support / (support + confidence )) * len(x)

print(sup)

# Initial (M,1)

for z in x:
    for z1 in set(z):
        y[z1] += 1

y1 = defaultdict(int)

# IF > SUP
for z in y.items():
    if z[1] > sup:
        y1[z[0]] = z[1]

print(y1.items())

z = defaultdict(int)

# m = 2
#Second iteration
for i in combinations(y1.keys(), 2):
    val = i[0] + i[1]
    for m in x:
        if containsall(m, val):
            z[val] += 1

# print(z.items())

# IF > SUP
z1 = defaultdict(int)
for k in z.items():
    if k[1] > sup:
        z1[k[0]] = k[1]

print(z1.items())

z4 = defaultdict(int)
# 4th iteration
for i in combinations(y1.keys(), 3):
    val = i[0] + i[1] + i[2]
    for m in x:
        if containsall(m, val):
            z4[val] += 1

# print(z4.items())
# IF > SUP
z4_1 = defaultdict(int)
for k in z4.items():
    if k[1] > sup:
        z4_1[k[0]] = k[1]

print("Z4_1: ", end="")

l = defaultdict(int)
print(z4_1.keys())
for kk in z4_1.keys():
    for i in permutations(kk):
        if i[0] not in l.keys():
            l[i[0]] = ''.join(i[1:])


print(l.items())

association = defaultdict(int)

for kk in z4_1.items():
    for i in l.items():
        supp = kk[1]
        conf = kk[1] / y[i[0]]
        association[i[0] + ", " + ''.join(i[1])] = (supp, conf, conf * 100)

print(association.items())

for s in association.items():
    if s[1][2] > confidence:
        print(s)


# Rewritten

def checksup(y, sup):
    y1 = defaultdict(int)
    for z in y.items():
        if z[1] > sup:
            y1[z[0]] = z[1]
    return y1

def permute(x):
    y = defaultdict(int)
    for z in x:
        for z1 in set(z):
            y[z1] += 1
    return y

def permute2(arr, x, n):
    z = defaultdict(int)
    for i in combinations(x.keys(), n):
        val = ''.join(i)
        for m in arr:
            if containsall(m, val):
                z[val] += 1
    # print()
    return z

def label(x):
    l = defaultdict(int)
    for kk in x.keys():
        for i in permutations(kk):
            if i[0] not in l.keys():
                l[i[0]] = ''.join(i[1:])
    return l

def makeassoc(z, label, first):
    assoc = defaultdict(int)
    for kk in z.items():
        for i in label.items():
            supp = kk[1]
            conf = kk[1] / first[i[0]]
            assoc[i[0] + " -> " + '^'.join(i[1])] = (supp, conf, conf * 100)
    return assoc

def filterassoc(assoc, confidence):
    finalassoc = defaultdict(int)
    for s in assoc.items():
        if s[1][2] > confidence:
            finalassoc[s[0]] = s[1]
    return finalassoc


def apriori(x, support=60, confidence=80):
    sup = (support / ( support + confidence )) * len(x)
    di = permute(x)
    newdi = checksup(di, sup)

    key = False

    mydi = newdi.copy()
    print(newdi)
    i = 2
    while key == False:
        print("Iteration: {0}".format(i))
        kk = permute2(x, di, i)
        di3 = checksup(kk, sup)
        print(di3.items())
        i += 1
        if len(di3) != 0:
            mydi = di3.copy()
            continue
        key = True
    di3 = mydi.copy()
    l = label(di3)
    assoc = makeassoc(di3, l, di)
    return filterassoc(assoc, confidence)

x = ["MONKEY", "DONKEY", "MAKE", "MUCKY", "COOKE"]

X = list(range(1, 11))

def comp(a, b):
    return str(a) + str(b)

# random.seed(2)
Y = [reduce(comp, set(reduce(comp, [random.randint(1, 10) for _ in range(random.randint(8, 10))]))) for _ in range(100)]

if __name__ == "__main__":
    # print(apriori(x, 60, 80))
    # print(Y)
    # random.seed(0)
    print(Y)
    print(apriori(Y, 60, 80))
