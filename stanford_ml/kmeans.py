#!/usr/bin/env python3

from functools import partial, reduce
import math
import random
from matplotlib import pyplot as plt

def dot(v, w):
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def sum_of_squares(v):
    return dot(v, v)

def vector_subtract(v, w):
    return [v_i - w_i for v_i, w_i in zip(v, w)]

def squared_distance(v, w):
    return sum_of_squares(vector_subtract(v, w))

def vector_add(v, w):
    return [v_i + w_i for v_i, w_i in zip(v, w)]

vector_sum = partial(reduce, vector_add)

def scalar_multiply(c, v):
    return [c * v_i for v_i in v]

def vector_mean(vectors):
    l = len(vectors)
    return scalar_multiply(1 / l, vector_sum(vectors))

def classify(k, means, input):
    return min(range(k), key=lambda i: squared_distance(input, means[i]))

def km(k, inputs):
    centroids = random.sample(inputs, k)
    l = len(inputs)
    c = None
    for _ in range(100):
        _classify  = partial(classify, k, centroids)
        _c = list(map(_classify, inputs))
        if c == _c:
            return centroids
        c = _c
        for _k in range(k):
            k_points = [p for p, a in zip(inputs, c) if a == _k]
            if k_points:
                centroids[_k] = vector_mean(k_points)
    return centroids

def squared_error(k, inputs):
    clusterer_c = km(k, inputs)
    _classify = partial(classify, k, clusterer_c)
    assignments = list(map(_classify, inputs))
    return sum(squared_distance(input, clusterer_c[c])
                    for input, c in zip(inputs, assignments))

if __name__ == "__main__":
    print("KMeans")
    random.seed(1)
    inputs = [[-14,-5],[13,13],[20,23],[-19,-11],[-9,-16],[21,27],[-49,15],[26,13],[-46,5],[-34,-1],[11,15],[-49,0],[-22,-16],[19,28],[-12,-8],[-13,-19],[-41,8],[-11,-6],[-25,-9],[-18,-3]]
    print(km(3, inputs))
    ks = range(1, len(inputs) + 1)
    errors = [squared_error(i, inputs) for i in ks]
    print(errors)
    plt.plot(ks, errors)
    plt.xlabel('k')
    plt.ylabel('total squared error')
    plt.title('Total Error vs. # of Clusters')
    plt.show()
