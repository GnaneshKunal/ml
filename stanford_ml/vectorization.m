#/usr/bin/octave -q

% -- vectorized implementation --
% h_theta(x) = sumation[j=0, n](theta_j, x_j)
%   this can be basically implemented as theta' * x
%   theta transpose * x

 prediction = theta' * x;

% -- un-vectorized implementation --

 prediction = 0.0;
 for j = 1:n+1,
     prediction = prediction + theta_j(j) * x(j);
 end;

% -- gradient descent --

delta = (1 / m) * sum((theta' * x) - y) * x
theta = theta - alpha * delta
