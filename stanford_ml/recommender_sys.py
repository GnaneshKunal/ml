#!/usr/bin/env python3

import math
import random
from collections import defaultdict
# from reg import gradient_descent_multi, predict_multi
from copy import copy

movie_list = ['Love at last', 'Romance forever', 'Cute puppies of love', 'Nonstop car chases', 'Swords vs. Karate']
movie_features = [[0.9, 0], [1.0, 0.01], [0.99, 0], [0.1, 1.0], [0, 0.9]]
movie_f = [[1] + f for f in movie_features]
users = {
    1: [5, 5, '?', 0, 0],
    2: [5, '?', 4, 0, 0],
    3: [0, '?', 0, 5, 5],
    4: [0, 0, '?', 4, '?']
}
n_u = len(users)
n_m = len(movie_list)

user_with_movies = { u: list(zip(d, movie_f)) for u, d in users.items()}

def fil(user_w_f):
    arr = defaultdict(list)
    arr2 = defaultdict(list)
    # return {u: [(m, k)] for u, d in arr.items() for m, k in d if m == '?'}
    for u, d in user_w_f.items():
        for m, k in d:
            if m != '?':
                arr[u].append((m, k))
            else:
                arr2[u].append((m, k))
    return arr, arr2

users_filtered, users_predict = fil(user_with_movies)

def dot(v, w):
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def predict_multi(x_i, beta):
    return dot(x_i, beta)

# def error_multi(beta, x, y):
#     return y - predict_multi(x, beta)

def error_multi(beta, x, y):
    return predict_multi(x, beta) - y

def gradient_descent_multi(x, y, theta, learning_rate=0.001):
    theta_temp = theta
    # for
    for m in range(10000):
        for x_i, y_i in zip(x, y):
            i = 0
            theta_temp_i = copy(theta_temp)
            for x_ii, theta_i in zip(x_i, theta_temp):
                theta_temp_i[i] = theta_i - learning_rate * (-1 * x_ii * (y_i - predict_multi(x_i, theta_temp)))
                i += 1
            theta_temp = copy(theta_temp_i)
    theta = copy(theta_temp)
    return theta

# def cost_function_multi(theta, x, y):
#     return (1 / (2 * len(x))) * sum(error_multi(theta, x_i, y_i) ** 2
#                 for x_i, y_i in zip(x, y))

def sum_features(x, has_one=True):
    k = 0
    for x_i in x:
        if has_one:
            for i in range(1, len(x_i)):
                k += math.pow(x_i[i], 2)
        else:
            for i in range(len(x_i)):
                k += math.pow(x_i[i], 2)
    return k

def cost_function_multi(theta, x, y, l):
    return 1 / 2 * sum(error_multi(theta, x_i, y_i) ** 2
                    for x_i, y_i in zip(x, y)) + (l / 2) * sum_features(x)

def cost_function_multi_main(theta, x, y, l):
    """theta is a [[], []]"""
    return sum(cost_function_multi(t, x, y, l) for t in theta)

def cost_function_multi_colab(theta, x, y, l):
    return 1 / 2 * sum(error_multi(theta, x_i, y_i) ** 2
                    for x_i, y_i in zip(x, y)) + (l / 2) * sum_features(x) * sum_features(theta)

def cost_function_multi_colab_main(theta, x, y, l):
    """theta is a [[], []]"""
    return sum(cost_function_multi_colab(t, x, y, l) for t in theta)

likings = [[0, 5, 0], [0, 5, 0], [0, 0, 5], [0, 0, 5]]

users_genre_liking = {i : l for i in range(1, 5) for l in likings}

x_p = [[random.random() for _ in range(len(movie_f[0]))] for _ in range(len(users))]

# Y = list(users.values())

def vector_sub(v, u):
    return [v_i - u_i  for v_i, u_i in zip(v, u)]

def sum_of_squares(v):
    return dot(v, v)

def squared_distance(v, w):
    return sum_of_squares(vector_sub(v, w))

def similarity(x_i, x_j):
    return math.sqrt(squared_distance(x_i, x_j))

# print(Y)
# movie_ratings_mean = [sum(y) * (1 / len(y) for y in Y]


if __name__ == "__main__":
    print("Recommender Systems")
    print(user_with_movies)
    print(users_filtered)
    print(users_predict)
    print("CONTENT BASED RECOMMENDATIONS")
    random.seed(0)
    theta = [[random.random() for _ in range(len(movie_f[0]))] for _ in range(len(users))]
    theta_new = []
    print("TRAIN")
    for u, d, t in zip(users_filtered.keys(), users_filtered.values(), theta):
        # print(d)
        x = []
        y = []
        for m, k in d:
            x.append(k)
            y.append(m)
        t_new = gradient_descent_multi(x, y, t)
        theta_new.append(t_new)
    print("Theta _ NEW")
    print(theta_new)
    print("PREDICT")
    for u, d, t in zip(users_predict.keys(), users_predict.values(), theta_new):
        print("User ", u)
        results = []
        for m, k in d:
            print(k, t)
            results.append(predict_multi(k, t))
    print(results)
    print("COLABORATIVE FILTERING")
    theta = x_p = [[random.random() for _ in range(len(movie_f[0]))] for _ in range(len(users))]

    # print(users_genre_liking)

    # for _ in range(1000):
        # for u, d, t in zip(users_genre_liking.keys(), users_genre_liking.values(), x_p):
            # print(d)
            # x = []
            # y = []
            # for m, k in d:
            #     x.append(k)
            #     y.append(m)
            # t_new = gradient_descent_multi(x, y, t)
            # theta_new.append(t_new)
    # predicted_ratings = 

    # ratings_means = [sum(f_i) * (1 / len(f_i))
    #     for f in users.values()
    #     for f_i in filter(lambda x: x if x != '?' else '', f)]
    # print(ratings_means)
    # print(list(filter(lambda x: x if x != '?' else '', list(users.values()))))
    # print(ratings_mean)
    # print(users_filtered)
