#!/usr/bin/octave -q

a = 2
b = 5; % semicolon supressing ouput
disp(b);
c = pi
1 == 2 % false
disp(sprintf('2 decs: %0.2f', c))

b = 'hi'

% 3x2 matrix
A = [1 2; 3 4; 5 6];
disp(A)

v = [1 2 3]
v'

% start:inc-by:end
v = 1:0.1:2
w = (0:.5:2)'

c = 2 * ones(2, 3)
w = ones(1, 3)
z = zeros(1, 2)

% uniform dist
rand(4, 3)

% gaussian dist
randn(1, 3)

w = -6 + sqrt(10) * (randn(1, 10000));
% hist(w)

% identity matrix
eye(4)

sz = size(A)
size(sz)
size(A, 1)

v = [1 2 3 4];
length(v)

pwd

who
whos

clear v
whos

v = w(1:10)
whos
v

% save hello.mat v
clear
load mr_inputs.mat
x(1:10)
size(x)
v = x(1:10)
% save hello.mat -ascii
x(3, 2)
x(2, :) % every element along that row or column

x([1 3], :) % get every element from 1, 3 rows of x
x(2, :) = [1   41    9    0];
A = [1; 2; 3];
size(A)
A = [A, [4; 5; 6]]
size(A)

A(:) % put all elements of A into a single vector
A = [1 2; 3 4; 5 6];
B = [11 12; 13 14; 15 16];
C = [A B]
C = [A; B]
C = [1 1; 2 2]
A * C

A .* B % take each element of A and multiply with corresponding element of B

A .^ 2

v = [1; 2; 3]
1 ./ v

log(v)
exp(v)
abs([-1; 2; -3])
-v % -1 * v
v + ones(length(v), 1) % == v + 1
v'  % transpose
v''
val = max(v)
[val, ind] = max(v)
v < 2 % does an element wise comparision and returns a vector
find(v <= 2) % returns the elements which passes the condition
V = magic(3)
v = [1 0; 2 4; 1 4]
[r, c] = find(v <= 2) % returns the row and column nums
a = sum(v)
prod(v)
ceil(v)
floor(v)

max(rand(3), rand(3))
max(V, [], 1)   % takes column wise max
max(V(:))       % returns the max element

A = magic(9)
sum(A, 2)
eye(9)
A .* eye(9)

A = magic(3)
temp = pinv(A)
temp * A

t = [0:0.1:0.98]
% plot(t)

% imagesc(A)

v = zeros(10, 1);
for i = 1:10,
    v(i) = 2^i;
end;
v
indices = 1:10
indices
for i = indices,
    disp(i);
end;

i = 1
while i <= 5,
    v(i) = 100;
    i = i + 1;
end;
v
i = 1
while true,
    v(i) = 999;
    i = i + 1;
    if i == 6,
        break;
    end;
end;
v

if v(1) == 1,
    disp("the value is one");
elseif v(1) == 2,
    disp("the value is two");
else
    disp("the value is not one or two")
end;

squareThisNumber(5)
x = [1 1; 1 2; 1 3]
y = [1; 2; 3]
theta = [0;0]
costFunctionJ(x, y, theta)























