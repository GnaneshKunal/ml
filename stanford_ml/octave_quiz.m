#!/usr/bin/octave -q

% Q1
A = [1 2; 3 4; 5 6];
B = [1 2 3; 4 5 6];

C = A' + B
C = B * A

% Q2
A = [16 2 3 13; 5 11 10 8; 9 7 6 12; 4 14 15 1]
B = A(:, 1:2)
B = A(1:4, 1:2)

A = magic(10)
x = magic(10)(1, :)'

v = zeros(10, 1);
for i = 10
    for j = 10
        v(i) = v(i) + A(i, j) * x(j);
    end
end
v

v = A * x

a = magic(10)(1, :)
b = magic(10)(1, :)

z = 0;
for i = 1:7
    z = z + a(i) * b(i);
end
z

z = b * a
