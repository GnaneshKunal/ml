#!/usr/bin/env python3.6

import math
import random
from copy import copy

def dot(v, w):
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def vector_subtract(v, w):
    return [v_i - w_i for v_i, w_i in zip(v, w)]

def sigmoid(x):
    return 1 / (1 + math.exp(-x))

def neuron_output(network, input):
    return sigmoid(dot(network, input))

predict = neuron_output

and_network = [-30, 20, 20]

or_network = [-10, 20, 20]

neg_network = [10, -20]

# n_a_n_network = [[10, -20], and_network]
n_a_n_network = [10, -20, -20]

xor_network = [[[-30, 20, 20],
                [-10, 20, 20]],
               [[-30, -60, 60]]]

xnor_network = [[[-30, 20, 20],
                [10, -20, -20]],
                [[-10, 20, 20]]]

# x = [[1, 2, 3], 
#      [1, 4, 2],
#      [0, 1, 4],
#      [1, -4, -1]]

# y = [[1, 0, 0, 0],
#      [0, 1, 0, 0],
#      [0, 0, 1, 0],
#      [0, 0, 0, 1]]

# theta = [
#     [[1, 2, 2, 2],
#      [1, 9, 2, 2],
#      [1, 9, 2, 2],
#      [1, 9, 2, 2],
#      [1, 9, 2, 2],
#      [1, 9, 2, 2]],
#      [[1, 0, 3, 0, 1, 0, 1],
#       [1, 0, 3, 0, 1, 0, 1],
#       [1, 0, 3, 0, 1, 0, 1],
#       [1, 0, 3, 0, 1, 0, 1],
#       [1, 0, 3, 0, 1, 0, 1],
#       [1, 0, 3, 0, 1, 0, 1]],
#      [[1, 2, 3, 1, 2, 1, 2],
#       [1, 2, 3, 1, 2, 1, 2],
#       [1, 2, 3, 1, 2, 1, 2],
#       [1, 2, 3, 1, 2, 1, 2]]
# ]

def transpose(m):
    return list(map(list, zip(*m)))

def feed_forward_neural(neural_network, input):
    outputs = []

    for layer in neural_network:
        input = [1] + input
        output = [neuron_output(neuron, input) for neuron in layer]
        outputs.append(output)
        input = output
    return outputs

# d = feed_forward_neural(theta, [1, 2, 3])

# print(d)
# out = d[-1]
# print("output layer", out)

# del_out = [a_i - y_i for a_i, y_i in zip(out, y[0])]
# print("del, out", del_out)

# t3 = transpose(theta[-1])

# print(t3)
# print("t3", t3)

# del_three = []

# # for i in range(7):
# for i in range(1, 7):
#     o = []
#     for j in range(4):
#         o.append(t3[i][j] * del_out[j] * (d[-2][j] * (1 - d[-2][j])))
#     del_three.append(sum(o))

# print("del three", del_three)
# t2 = transpose(theta[-2])
# print("t2", t2)
# del_two = []

# for i in range(7):
#     o = []
#     for j in range(6):
#         o.append(t2[i][j] * del_three[j] * (d[-3][j] * (1 - d[-3][j])))
#     del_two.append(sum(o))

# print("del two", del_two)

def cost_logistic(theta, x, y):
    return -y * math.log(predict(theta, x)) - (1 - y) * math.log(1 - predict(theta, x))

# down is the actual cost function
def maximum_likelihood_estimation(theta, x, y):
    return sum((1 / len(x)) * cost_logistic(theta, x_i, y_i)
                                for x_i, y_i in zip(x, y))

def theta_reg(theta, num_layer):
    sum = 0
    for l in range(num_layer - 1):
        for i in range(len(theta[l])):
            for j in range(len(theta[l][i])):
                sum += theta[l][i][j] ** 2
    return sum

def neural_cost_function(theta, x, y, network,reg_rate=10):
    return (1 / len(x)) * () + (10 / 2 * len(x)) *  sum(theta)

raw_digits = [
        """11111
            1...1
            1...1
            1...1
            11111""",

        """..1..
            ..1..
            ..1..
            ..1..
            ..1..""",

        """11111
            ....1
            11111
            1....
            11111""",

        """11111
            ....1
            11111
            ....1
            11111""",

        """1...1
            1...1
            11111
            ....1
            ....1""",

        """11111
            1....
            11111
            ....1
            11111""",

        """11111
            1....
            11111
            1...1
            11111""",

        """11111
            ....1
            ....1
            ....1
            ....1""",

        """11111
            1...1
            11111
            1...1
            11111""",

        """11111
            1...1
            11111
            ....1
            11111"""]

def make_digit(raw_digit):
    return [1 if c == '1' else 0
            for row in raw_digit.split("\n")
        for c in row.strip()]

inputs = list(map(make_digit, raw_digits))

targets = [[1 if i == j else 0 for i in range(10)]
            for j in range(10)]

def backpropagate(network, inputs, targets):
    hidden_outputs, outputs = feed_forward_neural(network, inputs)
    # output_deltas = []
    # L = len(outputs)
    # for i in reversed(range(L)):
    #     if i == L:
    #         output_deltas.append(vector_subtract(outputs[i], targets[i]))
    #     else:
    output_deltas = [output * (1 - output) * (output - target)
                            for output, target in zip(outputs, targets)]
    

if __name__ == "__main__":
    print("Neural Networks")
    print(feed_forward_neural(xor_network, [0, 1])[-1])

    x = [0, 1]
    theta = [[-30, 20, 20],
             [-10, 20, 20]]
    z = [dot(theta_i, [1] + x) for theta_i in theta]
    a = [sigmoid(z_i) for z_i in z]
    print(z)
    print(a)
    o_layer = [sigmoid(dot(theta[0], [1] + a))]
    print(o_layer)
    input_sample = [1, 0]
    print(neuron_output([1] + input_sample, and_network))
    print(neuron_output([1] + input_sample, or_network))
    print(neuron_output([1] + [0], neg_network))
    print(neuron_output([1] + [0, 0], n_a_n_network))
    for x in [0, 1]:
        for y in [0, 1]:
            print(x, y, feed_forward_neural(xnor_network, [x, y]))

    inputs2 = copy(inputs)
    targets2 = copy(targets)

    # print(inputs2)
    # print(targets2)

    in_l = [[random.random() for _ in range(26)] for _ in range(5)]
    # print(in_l)
    ou_l = [[random.random() for _ in range(6)] for _ in range(10)]
    # print(output_l)
    neural_network = [in_l, ou_l]
    h, o = feed_forward_neural(neural_network, inputs2[0])
    print("o", o)
    delta3 = [o_i - y_i for o_i, y_i in zip(o, targets2[0])]
    print("delta3", delta3)

    theta2 = transpose(ou_l)
    print("theta2", theta2)
    delta2 = []
    for i in range(1, 6):
        o_out = []
        for j in range(10):
            o_out.append(theta2[i][j] * delta3[j] * (o[j] * (1 - o[j])))
        delta2.append(sum(o_out))
    print(delta2)
