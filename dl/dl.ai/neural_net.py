#!/usr/bin/env python3

import numpy as np
from logistic_reg import sigmoid

def relu(x):
    return max(0, x)

def leaky_relu(x):
    return max(0.01 * x, x)

def tanh(x):
    return (np.exp(x) - np.exp(-x)) / (np.exp(x) + np.exp(-x))

def activation(x):
    # return sigmoid(x)
    return np.tanh(x)

# xor_network = [ # hidden layer
#                 [[20, 20, -30],     # 'and' neuron
#                  [20, 20, -10]],    # 'or' neuron
#                 # output layer
#                 [[-60, 60, -30]]]   # '2nd input but not 1st input' neuron


X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]]).T
Y = np.array([1, 0, 0, 1])

xor_network = np.array([np.array([[20, 20],
                        [20, 20]]),
                        np.array([[-60, 60]])])

W = xor_network
b = np.array([
    np.array([[-30, -10]]).T,
    np.array([-30]).T
])
# print(W.shape)
# print(b.shape)

def feed_forward(W, b, X):
    A = X
    l = W.shape[0]
    for i in range(l):
        # print(i)
        Z = W[i].dot(A) + b[i]
        if i == l - 1:
            A = sigmoid(Z)
        else:
            A = np.tanh(Z)
    return A

def back_propagate(W, b, X, Y, A, Z):
    m = X.shape[1]
    for i in reversed(range(W.shape[0])):
        #2
        dz = A[i] - Y
        dw = (1 / m) * dz.dot(A[i - 1].T)
        db = (1 / m) * np.sum(dz, axis=1, keepdims=True)
        #1
        dz = W[i].T.dot(dz) * sigmoid(Z[i]) (1 - sigmoid(Z[i]))
        dw = (1 / m) * dz.dot(X.T)
        db = (1 / m) * np.sum(dz, axis=1, keepdims=True)
    return dz, dw, db


def back_propagate_2(W, b, X, Y, A, Z):
    m = X.shape[1]
    l = W.shape[0]
    dz = A[l] - Y
    dw = (1 / m) * dz.dot(A[l - 1].T)
    db = (1 / m) * np.sum(dz, axis=1, keepdims=True)
    for i in reversed(range(W.shape[0])):
        dz = W[i].T.dot(dz) * sigmoid(Z[i]) (1 - sigmoid(Z[i]))
        dw = (1 / m) * dz.dot(X.T)
        db = (1 / m) * np.sum(dz, axis=1, keepdims=True)
    return dz, dw, db

A0 = X
Z1 = xor_network[0].dot(A0) + np.array([[-30, -10]]).T
# A1 = sigmoid(Z1)
A1 = activation(Z1)
Z2 = xor_network[1].dot(A1) + np.array([[-30]])
# A2 = sigmoid(Z2)
A2 = sigmoid(Z2)
print(A2)
print(feed_forward(W, b, X))

print(feed_forward(W, b, np.array([[1, 0]]).T))