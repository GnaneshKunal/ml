#!/usr/bin/env python3

import math
import cv2
import numpy as np

def im2fv(img):
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    dim = (400, 400)
    img_resized = cv2.resize(img_rgb, dim, interpolation=cv2.INTER_AREA)
    r, g, b = cv2.split(img_resized)
    return np.array((r.ravel(), g.ravel(), b.ravel())).ravel()

# X should be, such that X.shape == (nx, m)
# Y should be, such that Y.shape == (1, m)

def sigmoid(x):
    return (1 / 1 + math.exp(-x))

def predict(theta, b, x):
    return sigmoid(x.dot(theta) + b)

def error_function(y_hat, y):
    return - (y * math.log(y_hat) + (1 - y) * math.log(1 - y_hat))

def cost_function(x, y, theta, b):
    return (1 / len(x)) * np.sum(
        error_function(predict(theta, b, x_i), y_i) for x_i, y_i in zip(x, y)
    )

def d_error(a, y):
    return - (y / a) + ((1 - y) / (1 - a))

def stochastic_gradient_descent_np(x, y, theta, b, learning_rate=0.001):
    for _ in range(1500):
        for x_i, y_i in zip(x, y):
            theta = theta - learning_rate * (predict(x_i, b, theta) - y_i) * x_i
    return theta
