#!/usr/bin/env python3

import os
import numpy as np
import cv2
import random

# dir = os.getcwd() + "/../data/dogs-vs-cats/train5/"
# cat_dog = os.listdir(dir)
# dir2 = os.getcwd() + "/../data/dogs-vs-cats/train3/"
# cat_dog_t = os.listdir(dir2)

# dir = os.getcwd() + "/../data/seefood/train/hot_dog/"
# hot_dog_x = os.listdir(dir)
# cats_x = hot_dog_x
# dogs_x = os.listdir(os.getcwd() + "/../data/seefood/train/not_hot_dog/")
# print(dogs_x)
# cat_dog_t = os.listdir(os.getcwd() + "/../data/seefood/test/hot_dog/") + os.listdir(os.getcwd() + "/../data/seefood/test/not_hot_dog/")
# def split_cat_dog(arr):
#     cat = []
#     dog = []
#     for x in arr:
#         if 'cat' in x:
#             cat.append(x)
#         else:
#             dog.append(x)
#     return cat, dog
hot_dog_x = os.listdir(os.getcwd() + "/../data/seefood/train/hot_dog/")
not_hot_dog_x = os.listdir(os.getcwd() + "/../data/seefood/train/not_hot_dog/")

hot_dog_x_test = os.listdir(os.getcwd() + "/../data/seefood/test/hot_dog/")
not_hot_dog_x_test = os.listdir(os.getcwd() + "/../data/seefood/test/not_hot_dog/")

def image2vector(image):
    v = image.reshape((image.shape[0] * image.shape[1] * image.shape[2], 1))
    return v

def im2fv(img_name):
    print(img_name)
    img = cv2.imread(img_name)
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    dim = (64, 64)
    img_resized = cv2.resize(img_rgb, dim, interpolation=cv2.INTER_AREA)
    # r, g, b = cv2.split(img_resized)
    # return np.array((r.ravel(), g.ravel(), b.ravel())).ravel()
    return image2vector(img_resized)


def sigmoid(z):
    s = 1 / (1 + np.exp(-z))
    return s

def initialize_with_zeros(dim):
    w = np.zeros((dim, 1))
    b = 0
    return w, b

def propagate(w, b, X, Y):
    m = X.shape[1]
    A = sigmoid(w.T.dot(X) + b)
    cost = -(1 / m) * np.sum(Y * np.log(A) + (1 - Y) * np.log(1 - A))
    dw = (1 / m) * X.dot((A - Y).T)
    # print('prop', dw.shape)
    db = (1 / m) * np.sum(A - Y)
    cost = np.squeeze(cost)
    grads = {"dw": dw,
             "db": db}
    
    return grads, cost

def optimize(w, b, X, Y, num_iterations, learning_rate, print_cost = False):
    costs = []
    print('op-1', w.shape)
    for i in range(num_iterations):
        # print('op'+str(i), w.shape)
        grads, cost = propagate(w, b, X, Y)

        dw = grads["dw"]
        db = grads["db"]

        # print(dw.shape)
        
        w = w - learning_rate * dw
        b = b - learning_rate * db
        
        if i % 100 == 0:
            costs.append(cost)
        
        if print_cost and i % 100 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
    
    params = {"w": w,
              "b": b}

    grads = {"dw": dw,
             "db": db}
    
    return params, grads, costs

def predict(w, b, X):
    m = X.shape[1]
    Y_prediction = np.zeros((1,m))
    w = w.reshape(X.shape[0], 1)
    
    A = sigmoid(w.T.dot(X))

    for i in range(A.shape[1]):
        
        Y_prediction = np.array(A >= 0.5, dtype = np.float64)
    assert(Y_prediction.shape == (1, m))
    
    return Y_prediction

def model(X_train, Y_train, X_test, Y_test, num_iterations = 2000, learning_rate = 0.5, print_cost = False):
# def model(X_train, Y_train, num_iterations = 2000, learning_rate = 0.5, print_cost = False):

    w, b = initialize_with_zeros(X_train.shape[0])
    # print('model', w.shape)

    parameters, grads, costs = optimize(w, b, X_train, Y_train, num_iterations, learning_rate, print_cost)
    
    w = parameters["w"]
    b = parameters["b"]
    
    Y_prediction_test = predict(w, b, X_test)
    Y_prediction_train = predict(w, b, X_train)

    print("train accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_train - Y_train)) * 100))
    print("test accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_test - Y_test)) * 100))

    
    d = {"costs": costs,
         "Y_prediction_test": Y_prediction_test, 
         "Y_prediction_train" : Y_prediction_train, 
         "w" : w, 
         "b" : b,
         "learning_rate" : learning_rate,
         "num_iterations": num_iterations}
    
    return d

if __name__ == "__main__":

    hot_dog_y, not_hot_dog_y = np.zeros((len(hot_dog_x), 1)), np.ones((len(not_hot_dog_x), 1))
    hot_dogs = np.zeros((1, 12288))
    not_hot_dogs = np.zeros((1, 12288))

    for x in range(len(hot_dog_x)):
        # print("/../data/seefood/train/hot_dog/" + hot_dog_x[x])
        hot_dogs = np.vstack((hot_dogs, im2fv('/home/monster/git/ml/data/seefood/train/hot_dog/'+ hot_dog_x[x]).reshape(-1)))
    hot_dogs = np.delete(hot_dogs, 0, axis=0)
    print("CAT DONE")
    for x in range(len(not_hot_dog_x)):
        # print("/../data/seefood/train/not_hot_dog/" + not_hot_dog_x[x])
        not_hot_dogs = np.vstack((not_hot_dogs, im2fv('/home/monster/git/ml/data/seefood/train/not_hot_dog/' + not_hot_dog_x[x]).reshape(-1)))
    not_hot_dogs = np.delete(not_hot_dogs, 0, axis=0)
    
    hot_not_dog = np.vstack((hot_dogs, not_hot_dogs))
    print(hot_dog_y.shape)
    print(not_hot_dog_y.shape)
    hot_not_dog_y = np.vstack((hot_dog_y, not_hot_dog_y))

    # TEST

    # hot_dog_x_test 
    # not_hot_dog_x_test
    hot_dog_y_test, not_hot_dog_y_test = np.zeros((len(hot_dog_x_test), 1)), np.ones((len(not_hot_dog_x_test), 1))

    hot_dogs_test = np.zeros((1, 12288))
    not_hot_dogs_test = np.zeros((1, 12288))

    for x in range(len(hot_dog_x_test)):
        hot_dogs_test = np.vstack((hot_dogs_test, im2fv('/home/monster/git/ml/data/seefood/test/hot_dog/' + hot_dog_x_test[x]).reshape(-1)))
    hot_dogs_test = np.delete(hot_dogs_test, 0, axis=0)
    for x in range(len(not_hot_dog_x_test)):
        not_hot_dogs_test = np.vstack((not_hot_dogs_test, im2fv('/home/monster/git/ml/data/seefood/test/not_hot_dog/' + not_hot_dog_x_test[x]).reshape(-1)))
    not_hot_dogs_test = np.delete(not_hot_dogs_test, 0, axis=0)

    hot_not_dog_test = np.vstack((hot_dogs_test, not_hot_dogs_test))
    hot_not_dog_y_test = np.vstack((hot_dog_y_test, not_hot_dog_y_test))

    # -----------

    np.random.seed(x)
    np.random.shuffle(hot_not_dog)
    np.random.seed(x)
    np.random.shuffle(hot_not_dog_y)
    train_set_x = hot_not_dog.T / 255
    train_set_y = hot_not_dog_y.T

    ## TEST 

    np.random.seed(x)
    np.random.shuffle(hot_not_dog_test)
    np.random.seed(x)
    np.random.shuffle(hot_not_dog_y_test)
    test_set_x = hot_not_dog_test.T / 255
    test_set_y = hot_not_dog_y_test.T

    # ----
    print(train_set_x.shape)
    d = model(train_set_x, train_set_y, test_set_x, test_set_y, num_iterations = 2000, learning_rate = 0.005, print_cost = True)
    print(d)
    theta = d["w"]
    b = d["b"]
    np.save('seefood', theta)
    print(test_set_y)
    print(predict(
        theta, b,
        test_set_x
    ))
