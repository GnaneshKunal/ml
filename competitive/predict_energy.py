#!/usr/bin/env python3

import csv
import random
import math

# def dot(v, w):
#     """v_1 * w_1 + ... + v_n * w_n"""
#     return sum(v_i * w_i for v_i, w_i in zip(v, w))

# def predict(x_i, beta):
#     """assumes that the first element of each x_i is 1"""
#     return dot(x_i, beta)

def train_data(header=True):
    with open('../data/ae5e3a76-0-energy/train.csv') as f:
        x = []
        y = []
        reader = csv.reader(f, delimiter=',')
        if header:
            next(reader, None)
        for row in reader:
            # x.append([1] + [float(row[i])for i in range(1, len(row) - 1)])
            x.append([float(row[i])for i in range(1, len(row) - 1)])
            y.append(float(row[-1]))
    return x, y

def test_data(header=True):
    with open('../data/ae5e3a76-0-energy/test.csv') as f:
        x = []
        y = []
        o = []
        reader = csv.reader(f, delimiter=',')
        if header:
            next(reader, None)
        for row in reader:
            o.append(int(row[0]))
            # x.append([1] + [float(row[i])for i in range(1, len(row) - 1)])
            x.append([float(row[i])for i in range(1, len(row))])
            y.append(float(row[-1]))
    return o, x, y

# def error(x_i, y_i, beta):
#     return y_i - predict(x_i, beta)

# def squared_error(x_i, y_i, beta):
#     return error(x_i, y_i, beta) ** 2

# def squared_error_gradient(x_i, y_i, beta):
#     """the gradient (with respect to beta)
#     corresponding to the ith squared error term"""
#     return [-2 * x_ij * error(x_i, y_i, beta)
#             for x_ij in x_i]

# def in_random_order(data):
#     """generator that returns the elements of data in random order"""
#     indexes = [i for i, _ in enumerate(data)]   # create a list of indexes
#     random.shuffle(indexes)
#     for i in indexes:                           # return the data in that order
#         yield data[i]

# def vector_subtract(v, w):
#     """subtracts corresponding elements"""
#     return [v_i - w_i
#             for v_i, w_i in zip(v, w)]

# def scalar_multiply(c, v):
#     """c is a number, v is a vector"""
#     return [c * v_i for v_i in v]

# def minimize_stochastic(target_fn, gradient_fn, x, y, theta_0, alpha_0=0.01):

#     data = list(zip(x, y))          # initial guess
#     theta = theta_0                 # initial step size
#     alpha = alpha_0                 # the minimum so far
#     min_theta, min_value = None, float('inf')
#     iterations_with_no_improvement = 0

#     # if we ever go 100 iterations with no improvement, stop
#     while iterations_with_no_improvement < 100:
#         print(iterations_with_no_improvement)
#         value = sum(target_fn(x_i, y_i, theta) for x_i, y_i in data)
#         if value < min_value:
#             # if we've found a new minimum, remember it
#             # and go back to the original step size
#             min_theta, min_value = theta, value
#             iterations_with_no_improvement = 0
#             alpha = alpha_0
#         else:
#             # otherwise we're not improving, so try shrinking the step size
#             iterations_with_no_improvement += 1
#             alpha *= 0.9
        
#         # and take a gradient step for each of the data points
#         for x_i, y_i in in_random_order(data):
#             gradient_i = gradient_fn(x_i, y_i, theta)
#             theta = vector_subtract(theta, scalar_multiply(alpha, gradient_i))

#     return min_theta

# def estimate_beta(x, y):
#     beta_initial = [random.random() for x_i in x[0]]
#     return minimize_stochastic(squared_error,
#                                 squared_error_gradient,
#                                 x, y,
#                                 beta_initial,
#                                 0.001)

# def shape(A):
#     num_rows = len(A)
#     num_cols = len(A[0]) if A else 0
#     return num_rows, num_cols

# def get_row(A, i):
#     return A[i]

# def get_col(A, j):
#     return [A_i[j] for A_i in A]

# def mean(x):
#     return sum(x) / len(x)


# def de_mean(x):
#     x_bar = mean(x)
#     # print(x_bar)
#     return [x_i - x_bar for x_i in x]

# def sum_of_squares(v):
#     return dot(v, v)

# def variance(x):
#     n = len(x)
#     deviations = de_mean(x)
#     return sum_of_squares(deviations) / (n - 1)

# def standard_deviation(x):
#     return math.sqrt(variance(x))

# def make_matrix(num_rows, num_cols, entry_fn):
#     return [[entry_fn(i, j)
#                 for j in range(num_cols)]
#                 for i in range(num_rows)]

# def scale(data_matrix):
#     num_rows, num_cols = shape(data_matrix)
#     means = [mean(get_col(data_matrix, j))
#                 for j in range(num_cols)]
#     stdevs = [standard_deviation(get_col(data_matrix, j))
#                 for j in range(num_cols)]
#     return means, stdevs

# def rescale(data_matrix):
#     means, stdevs = scale(data_matrix)

#     def rescaled(i, j):
#         if stdevs[j] > 0:
#             return (data_matrix[i][j] - means[j]) / stdevs[j]
#         else:
#             return data_matrix[i][j]
#     num_rows, num_cols = shape(data_matrix)
#     return make_matrix(num_rows, num_cols, rescaled)

# #!/usr/bin/env python3

import numpy as np

def sigmoid(z):
    # return 1 / (1 + np.exp(-z))
    return z

def initialize_with_zeros(dim):
    w = np.zeros((dim, 1))
    b = 0
    return w, b

def propagate(w, b, X, Y):
    m = w.shape[0]
    # A = sigmoid(w.T.dot(X) + b)
    A = w.T.dot(X) + b
    cost = -(1 / m) * np.sum(Y * np.log(A) + (1 - Y) * np.log(1 - A))

    # b = (y_i - theta_0_temp - theta_1_temp * x_i))
    # (x_i * (y_i - (theta_0_temp + theta_1_temp * x_i))))

    dw = (1 / m) * X.dot((A - Y).T)
    db = (1 / m) * np.sum(A - Y)
    cost = np.squeeze(cost)
    grads =  {
        "dw": dw,
        "db": db
    }
    return grads, cost

def optimize(w, b, X, Y, num_iterations, learning_rate, print_cost=False):
    costs = []
    for i in range(num_iterations):
        grads, cost = propagate(w, b, X, Y)

        dw = grads["dw"]
        db = grads["db"]

        w = w - learning_rate * dw
        b = b - learning_rate * db

        if i % 100 == 0:
            costs.append(cost)
        
        if print_cost and i % 100 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
    
    params = {
        "w": w,
        "b": b
    }

    grads = {
        "dw": dw,
        "db": db
    }
    return params, grads, costs

def predict(w, b, X):
    m = X.shape[1]
    print(w.shape)
    print(X.shape)
    # print(X[0])
    Y_prediction = np.zeros((1, m))
    # print(m)
    w = w.reshape((X.shape[0], 1))

    # A = sigmoid(w.T.dot(X) + b)
    A = w.T.dot(X) + b
    print("This is A", A)
    # for i in range(A.shape[1]):
        
        # Y_prediction = np.array(A >= 0.5, dtype = np.float64)
    print(Y_prediction.shape)
    assert(Y_prediction.shape == (1, m))
    
    return Y_prediction

def model(X_train, Y_train, X_test, Y_test, num_iterations = 2000, learning_rate = 0.5, print_cost = False):
    
    w, b = initialize_with_zeros(X_train.shape[0])

    parameters, grads, costs = optimize(w, b, X_train, Y_train, num_iterations, learning_rate, print_cost)
    
    w = parameters["w"]
    b = parameters["b"]
    
    Y_prediction_train = predict(w, b, X_train)
    Y_prediction_test = predict(w, b, X_test)

    print("train accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_train - Y_train)) * 100))
    print("test accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_test - Y_test)) * 100))

    
    d = {"costs": costs,
         "Y_prediction_test": Y_prediction_test, 
         "Y_prediction_train" : Y_prediction_train, 
         "w" : w, 
         "b" : b,
         "learning_rate" : learning_rate,
         "num_iterations": num_iterations}
    
    return d

if __name__ == "__main__":
    x, y = train_data()
    # x = rescale(x)
    train_set_x = np.array(x).T
    train_set_y = np.array(y).T
    print(len(x))
    print(len(x[0]))
    print(train_set_x.shape)
    print(train_set_x[:, -1])
    # theta = estimate_beta(x, y)
    # theta = [0.7618278752925345, 0.6755171065849577, 0.8760930775050291, 0.9336405999654118, 0.1967018961087117, 0.6941616058871856, 0.18632010065820603, 0.7115774519537399, 0.9063640404538063, 0.9815147682332459, 0.2673242556416018, 0.9732234107803148, 0.20217186159170475, 0.35458536985108235, 0.3916827786280175, 0.9436818010204722, 0.5079505633072146, 0.5184818671227394, 0.7778115124939617, 0.2698428982174842, 0.8593526000606205, 0.3897945415357892, 0.5626218592042666, 0.4196781850302128, 0.5629812255512834]
    # print(theta)
    # theta = [97.2898282867871, 2.852350905646977, 64.57073843711144, -43.918013514602954, -57.80510886288262, 48.50465293306777, 15.710298416051444, 5.43044786342158, 7.729068330782237, -1.0140972748735344, 2.3308985265833835, 45.18653380855278, 10.739445765549723, 0.49127897780726343, -8.315537889170722, 19.662656797077002, -30.884328912614894, -41.17151130298699, -7.086143718014544, -48.97161922856484, 0.7714113349584775, -9.594915614717706, 5.3240581308142465, 1.682138507796326, 15.251335242037221]
    o, x_test, y_test = test_data()
    o = np.array(o)
    test_set_x = np.array(x_test).T
    test_set_y = np.array(y_test).T
    # error = sum(squared_error(x_i, y_i, theta) for x_i, y_i in zip(x_test, y_test))
    # print(error)
    # writer = os.open('predict.txt', 'w')
    # j = rescale(x_test)
    # for o_i, x_i in zip(o, j):
        # print(o_i, ",", math.floor(predict(x_i, theta))) 
    # print(train_set_x)
    d = model(train_set_x, train_set_y, test_set_x, test_set_y, num_iterations = 2500, learning_rate = 0.00000000000000548, print_cost = True)
    # print(d)
    for o_i, x_i in zip(o, predict(d["w"], d["b"], test_set_x)):
        print(x_i)
        print(o_i, ",", math.floor(x_i)) 
    # print(train_set_x)



