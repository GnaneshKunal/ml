#!/usr/bin/env python3

import numpy as np
import torch

arr = [[1, 2], [3, 4]]
print(arr)

# Convert to NumPy
n_array = np.array(arr)
print(n_array)

t_tensor = torch.Tensor(arr)
print(t_tensor)
print(type(t_tensor))

# Create Matrices with Default Values

np_ones = np.ones((2, 2))
print(np_ones)

print(torch.ones((2, 2)))

print(np.random.rand(2, 2))
print(torch.rand(2, 2))

np.random.seed(0)
print(np.random.rand(2, 2))

np.random.seed(0)
print(np.random.rand(2, 2))
print(np.random.rand(2, 2))

# Torch Seed
torch.manual_seed(0)
print(torch.rand(2, 2))
torch.manual_seed(0)
print(torch.rand(2, 2))

# No seed
print(torch.rand(2, 2))

# Numpy array
np_array = np.ones((2, 2))
print(np_array)

print(type(np_array))

# Convert to Torch Tensor
torch_tensor = torch.from_numpy(np_array)
print(torch_tensor)
print(type(torch_tensor))

np_array_new = np.ones((2, 2), dtype=np.int32)
print(type(torch.from_numpy(np_array_new)))

# Torch to NumPy
torch_tensor = torch.ones(2, 2)
print(type(torch_tensor))

torch_to_numpy = torch_tensor.numpy()
print(type(torch_to_numpy))

# Tensor Operations
a = torch.ones(2, 2)
print(a.size())

print(a.view(4))
print(a.view(4).size())

# Element wise Addition
a = torch.ones(2, 2)
print(a)

b = torch.ones(2, 2)
print(b)

c = a + b
print(c)

c = torch.add(a, b)
print(c)

# In-Place Addition
print("Old c Tensor")
print(c)
c.add_(a)
print('-'*60)
print('New c Tensor')
print(c)

# Subtraction
print(a - b)

# Not in-place
print(a.sub(b))
print(a)

# Inplace
print(a.sub_(b))
print(a)

# Element-wise Multiplication
a = torch.ones(2, 2)
b = torch.zeros(2, 2)

print(a * b)

# Not in-place
print(torch.mul(a, b))
print(a)

# In-place
print(a.mul_(b))
print(b)

# Exact for Division as well.

# Tensor Mean

a = torch.Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9])
print(a.size())
print(a.mean(dim=0))

b = torch.Tensor([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]])
print(b.size())
print(b.mean(dim=1))

# Tensor Standard Deviation
c = torch.Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(c.std(dim=0))
